export class CalculatorButtonFunction implements ICalculatorButtonFunction{
    name!: string;
    parameter?: string;

    constructor(data?: ICalculatorButtonFunction) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ICalculatorButtonFunction {
    name: string;
    parameter?: string;
}
