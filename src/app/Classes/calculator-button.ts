import {CalculatorButtonFunction} from "./calculator-button-function";

export class CalculatorButton implements ICalculatorButton{
    rowSize!: string;
    btnClass!: string;
    btnText!: string;
    function!: CalculatorButtonFunction;

    constructor(data?: ICalculatorButton) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ICalculatorButton {
    rowSize: string;
    btnClass: string;
    btnText: string;
    function: CalculatorButtonFunction;
}
