import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CalculatorButton} from "../../Classes/calculator-button";

@Component({
    selector: 'calculator-button',
    templateUrl: './calculator-button.component.html',
    styleUrls: ['./calculator-button.component.css']
})
export class CalculatorButtonComponent implements OnInit {

    @Input() button: CalculatorButton = new CalculatorButton();
    @Output() onClick = new EventEmitter<CalculatorButton>();

    constructor() {
    }

    ngOnInit(): void {
    }

    clickFunction(button: CalculatorButton) {
        this.onClick.emit(button);
    }
}
