import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CalculatorButton} from "../../Classes/calculator-button";

@Component({
    selector: 'app-calculator-buttons',
    templateUrl: './calculator-buttons.component.html',
    styleUrls: ['./calculator-buttons.component.css']
})
export class CalculatorButtonsComponent implements OnInit {

    @Input() buttons: CalculatorButton[] = [];
    @Output() onClick = new EventEmitter<CalculatorButton>();

    constructor() {
    }

    ngOnInit(): void {
    }

    clickFunction(button: CalculatorButton) {
        this.onClick.emit(button);
    }
}
