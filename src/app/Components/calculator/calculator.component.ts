import {Component, OnInit} from '@angular/core';
import {CalculatorButton} from "../../Classes/calculator-button";
import {calculatorButtons} from 'src/assets/initCalculatorButtons';

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

    input: string = '';
    result: string = '';

    firstRow: CalculatorButton[] = calculatorButtons.firstRow;
    secondRow: CalculatorButton[] = calculatorButtons.secondRow;
    thirdRow: CalculatorButton[] = calculatorButtons.thirdRow;
    fourthRow: CalculatorButton[] = calculatorButtons.fourthRow;
    fifthRow: CalculatorButton[] = calculatorButtons.fifthRow;

    constructor() {
    }

    ngOnInit(): void {}

    getLastOperand() {
        let pos: number;
        pos = this.input.toString().lastIndexOf("+");

        if (this.input.toString().lastIndexOf("-") > pos) pos = this.input.lastIndexOf("-");
        if (this.input.toString().lastIndexOf("*") > pos) pos = this.input.lastIndexOf("*");
        if (this.input.toString().lastIndexOf("/") > pos) pos = this.input.lastIndexOf("/");

        return this.input.substr(pos + 1);
    }

    isOperand(key: string) {
        return key === '/' ||
            key === '*' ||
            key === '-' ||
            key === '+';
    }

    pressNum(num: string) {
        if (num == ".") {
            if (this.input != "") {
                const lastNum = this.getLastOperand();
                if (lastNum.lastIndexOf(".") >= 0) return;
            }
        }

        if (num == "0") {
            const prevKey = this.input[this.input.length - 1];

            if (
                this.isOperand(prevKey)
            ) {
                return;
            }
        }

        this.input = this.input + num;
        this.calcResult();
    }

    pressOperator(op: string) {
        const lastKey = this.input[this.input.length - 1];

        if (
            (this.input === '-' && lastKey === '-') ||
            (lastKey === undefined && op !== '-')
        ) {
            return;
        }

        if (
            this.isOperand(lastKey) &&
            this.isOperand(op)
        ) {
            this.input = this.input.substr(0, this.input.length - 1);
        }

        this.input = this.input + op;
        this.calcResult();
    }

    clear() {
        this.input = this.input.toString();

        if (this.input != "") {
            this.input = this.input.substr(0, this.input.length - 1);
        }

        this.result = this.input;
    }

    allClear() {
        this.result = '';
        this.input = '';
    }

    calcResult() {
        let formula = this.input;
        let lastKey = formula[formula.length - 1];

        if (formula === '') {
            return;
        }

        if (lastKey === '.') {
            formula = formula.substr(0, formula.length - 1);
        }

        lastKey = formula[formula.length - 1];

        if (
            this.isOperand(lastKey) ||
            lastKey === '.'
        ) {
            formula = formula.substr(0, formula.length - 1);
        }

        if (formula === undefined || formula === '') return;
        this.result = eval(formula);
    }

    getResult() {
        this.calcResult();
        this.input = this.result;
    }

    catchClickedFunction(button: CalculatorButton) {
        let buttonFunction = button.function;

        let name = buttonFunction.name;
        let parameter = buttonFunction.parameter;

        switch(name) {
            case 'allClear':
                this.allClear();
                break;
            case 'clear':
                this.clear();
                break;
            case 'pressOperator':
                this.pressOperator(parameter ?? '');
                break;
            case 'pressNum':
                this.pressNum(parameter ?? '');
                break;
            case 'getResult':
                this.getResult();
                break;
            default:
                this.allClear();
                break;
        }
    }
}
