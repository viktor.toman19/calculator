import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CalculatorComponent} from './Components/calculator/calculator.component';
import {CalculatorButtonsComponent} from './Components/calculator-buttons/calculator-buttons.component';
import { CalculatorButtonComponent } from './Components/calculator-button/calculator-button.component';

@NgModule({
    declarations: [
        AppComponent,
        CalculatorComponent,
        CalculatorButtonsComponent,
        CalculatorButtonComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
