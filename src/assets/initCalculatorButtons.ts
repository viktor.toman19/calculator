import {CalculatorButton} from "../app/Classes/calculator-button";
import {CalculatorButtonFunction} from "../app/Classes/calculator-button-function";

const zero = '0';
const one = '1';
const two = '2';
const three = '3';
const four = '4';
const five = '5';
const six = '6';
const seven = '7';
const eight = '8';
const nine = '9';

const division = '/';
const multiplication = '*';
const addition = '+';
const subtraction = '-';
const equal = '=';
const point = '.';

export const calculatorButtons = {
    firstRow: [
        new CalculatorButton({
            rowSize: 'col-sm-6',
            btnClass: 'btn-lg btn-danger',
            btnText: 'AC',
            function: new CalculatorButtonFunction({
                name: 'allClear'
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-warning',
            btnText: 'C',
            function: new CalculatorButtonFunction({
                name: 'clear'
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-secondary',
            btnText: division,
            function: new CalculatorButtonFunction({
                name: 'pressOperator',
                parameter: division
            })
        })
    ],
    secondRow: [
        new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: seven,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: seven
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: eight,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: eight
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: nine,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: nine
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-secondary',
            btnText: multiplication,
            function: new CalculatorButtonFunction({
                name: 'pressOperator',
                parameter: multiplication
            })
        })
    ],
    thirdRow: [
        new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: four,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: four
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: five,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: five
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: six,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: six
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-secondary',
            btnText: subtraction,
            function: new CalculatorButtonFunction({
                name: 'pressOperator',
                parameter: subtraction
            })
        })
    ],
    fourthRow: [
        new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: one,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: one
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: two,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: two
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: three,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: three
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-secondary',
            btnText: addition,
            function: new CalculatorButtonFunction({
                name: 'pressOperator',
                parameter: addition
            })
        })
    ],
    fifthRow: [
        new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: point,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: point
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-3',
            btnClass: 'btn-lg btn-light',
            btnText: zero,
            function: new CalculatorButtonFunction({
                name: 'pressNum',
                parameter: zero
            })
        }), new CalculatorButton({
            rowSize: 'col-sm-6',
            btnClass: 'btn-lg btn-success',
            btnText: equal,
            function: new CalculatorButtonFunction({
                name: 'getResult'
            })
        })
    ],
};
